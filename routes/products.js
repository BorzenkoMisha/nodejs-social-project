const express = require('express');
const mongoose = require('mongoose');
const Product = mongoose.model('Product');
const router = module.exports = express.Router();

router
  .get('/', (req, res, next) => {
    const query = {};
    if (req.query.category_id) {
      if (req.query.category_id === 'favorites'){
          query.isFavorite = true;
      } else {
        query.category_id = req.query.category_id;
      }
    }
    Product.find(query, (err, result) => (err ? next(err) : res.json(result)));
  })
  .post('/', (req, res, next) => {
    Product.create(req.body, (err, result) => (err ? next(err) : res.json(result)));
  })
  .delete('/:id', function(req,res,next) {
    Product.remove({_id: req.params.id}, (err, result) => (err ? next(err) : res.json(result)));
  })
  .put('/:id', function(req,res,next) {
    const product = req.body;
    Product.findOneAndUpdate({_id: req.params.id}, product, {new: true}, (err, result) => (err ? next(err) : res.json(result)));
  });
