const express = require('express');
const mongoose = require('mongoose');
const Category = mongoose.model('Category');
const Product = mongoose.model('Product');
const router = module.exports = express.Router();

router
  .get('/', (req, res, next) => {
    Category.find((err, result) => (err ? next(err) : res.json(result)));
  })
  .get('/:id', (req,res,next) => {
    Category.findOne({_id: req.params.id}, (err, result) => (err ? next(err) : res.json(result)));
  })
  .post('/', (req, res, next) => {
    const category = req.body;
    Category.create(category, (err, result) => (err ? next(err) : res.json(result)));
  })
  .delete('/:id', function(req,res,next) {
    Category.remove({_id: req.params.id}, (err) => {
      if(err){
        return next(err)
      }
      return Product.update({ category_id: req.params.id }, { $set: { category_id: '0' }}, {multi: true}, (err, result) => (err ? next(err) : res.json(result)));
    })
  })
  .put('/:id', function(req,res,next) {
    const category = req.body;
    category.isEdditing = false;
    Category.findOneAndUpdate({_id: req.params.id}, category, {new: true}, (err, result) => (err ? next(err) : res.json(result)));
  });
