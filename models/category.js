const mongoose = require('mongoose');

const schema = new mongoose.Schema({
  title: String,
  isEdditing: {type: Boolean, default: false}
},{collection: 'categories'});

mongoose.model('Category', schema, 'categories');
