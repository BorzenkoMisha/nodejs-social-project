const mongoose = require('mongoose');

const schema = new mongoose.Schema({
  url: String,
  type: {type: String, default: 'text'},
  isFavorite: {type: Boolean, default: false},
  category_id: {type: String, default: '0'},
  data: {
    title: {type: String, default: ''},
    description: {type: String, default: ''}
  }
},{collections: 'products'});

mongoose.model('Product', schema, 'products');
